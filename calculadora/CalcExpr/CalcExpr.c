#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "CalcExpr.h"

int controle_calculadora(char * string, PilhaInt * pilha_operacao, PilhaInt * pilha_numeros) {
  char str[100];
  snprintf(str, sizeof str, "(%s)", string);
  char item;
  int i, cnt, sum, isexpo, isnegative;
  isnegative = sum = 0;
  cnt = isexpo = i = 0;
  while (str[i] != '\n') {
    i++;
  }
  str[i] = '\0';
  i = 0;

  while ((item = str[i]) != '\0') {
    if (item == '(' && str[i + 1] == '-') {
      isnegative = 1;
      i++;
      continue;
    } else if (item == '(') {
      i++;
      continue;
    } else if (item == '+' || item == '-' || item == '*' || item == '/') {
      inserePilha_calculadora(pilha_operacao, item);
      cnt++;
    } else if (item >= 48 && item <= 57 && isnegative == 1) {
      isnegative = 0;
      inserePilha_calculadora(pilha_numeros, -(item - 48));
    } else if (item >= 48 && item <= 57) {
      inserePilha_calculadora(pilha_numeros, (item - 48));
    } else if (item == ')' && isexpo == 1) {
      cnt++;
      sum += logica_calculadora(pilha_numeros, pilha_operacao, cnt, isexpo);
      cnt = 0;
      isexpo = 0;
    } else if (item == ')') {
      sum += logica_calculadora(pilha_numeros, pilha_operacao, cnt, 0);
      cnt = 0;
    } else if (item == 'e' || item == 'x' || item == 'p' || item == 'o' || item == ',') {
      isexpo = 1;
    } else printf("\n%c e invalido.\n", item);
    i++;
  }
  return sum;
}

void inserePilha_calculadora(PilhaInt * pi, int item) {
  ElemInt e;
  e.valor = item;

  ElemInt * no;
  no = (ElemInt * ) malloc(sizeof(ElemInt));

  no -> valor = item;
  no -> prox = ( * pi);
  * pi = no;

}

int retornaNumeroPilha_calculadora(PilhaInt * pilha_num) {
  int topNumber;
  if (pilha_num == NULL)
    return 0;
  if (( * pilha_num) == NULL)
    return 0;
  topNumber = ( * pilha_num) -> valor;

  ElemInt * no = * pilha_num;
  * pilha_num = no -> prox;
  free(no);

  return topNumber;
}

int logica_calculadora(PilhaInt * pilha_num, PilhaInt * pilha_op, int count_op, int isexpo) {
  int num1, num2, ans;
  ans = 0;
  char sign;
  num1 = retornaNumeroPilha_calculadora(pilha_num);
  num2 = retornaNumeroPilha_calculadora(pilha_num);

  while (count_op != 0) {

    if (isexpo == 1) {
      ans = (pow(num2, num1));
      isexpo = 0;
      count_op--;

    } else {

      if (count_op > 0 && ans != 0) {
        int a = retornaNumeroPilha_calculadora(pilha_num);
        if (a == 0) {
          break;
        } else {
          num2 = a;
          num1 = ans;
        }
      }

      switch (retornaNumeroPilha_calculadora(pilha_op)) {
      case '+':
        ans = num1 + num2;
        sign = '+';
        break;
      case '-':
        ans = num1 - num2;
        sign = '-';
        break;
      case '*':
        ans = num1 * num2;
        sign = '*';
        break;
      case '/':
        ans = num1 / num2;
        sign = '/';
        break;
      }
    }
  }
  inserePilha_calculadora(pilha_num, ans);

  return ans;
}

PilhaInt * cria_PilhaInt() {
  PilhaInt * pi = (PilhaInt * ) malloc(sizeof(PilhaInt));
  if (pi != NULL)
    *
    pi = NULL;
  return pi;
}

Pilha * cria_Pilha() {
  Pilha * pi = (Pilha * ) malloc(sizeof(Pilha));
  if (pi != NULL)
    *
    pi = NULL;
  return pi;
}

void validacao_numerica(char * str) {

  PilhaInt * pi1 = cria_PilhaInt();
  PilhaInt * pi2 = cria_PilhaInt();

  int isValid = controle_calculadora(str, pi1, pi2);

  strtok(str, "\n");
  if (isValid != 0) {
    printf("A expressao E VALIDA \n\n\n", str);
  } else {
    printf("A expressao: %s E INVALIDA \n\n\n", str);
  }

}

void libera_Pilha(Pilha* pi){
    if(pi != NULL){
        Elem* no;
        while((*pi) != NULL){
            no = *pi;
            *pi = (*pi)->prox;
            free(no);
        }
        free(pi);
    }
}

int consulta_topo_Pilha(Pilha* pi, char *carac){
    if(pi == NULL)
        return 0;
    if((*pi) == NULL)
        return 0;
    *carac = (*pi)->valor;
    return 1;
}

char topo_pilha(Pilha* pi){
    if(pi == NULL)
        return ' ';
    if((*pi) == NULL)
        return ' ';
    return (*pi)->valor;
}

int insere_Pilha(Pilha* pi, char carac){
    if(pi == NULL)
        return 0;
    Elem* no;
    no = (Elem*) malloc(sizeof(Elem));
    if(no == NULL)
        return 0;
    no->valor = carac;
    no->prox = (*pi);
    *pi = no;
    return 1;
}

int remove_Pilha(Pilha* pi){
    if(pi == NULL)
        return 0;
    if((*pi) == NULL)
        return 0;
    Elem *no = *pi;
    *pi = no->prox;
    free(no);
    return 1;
}

int tamanho_Pilha(Pilha* pi){
    if(pi == NULL)
        return 0;
    int cont = 0;
    Elem* no = *pi;
    while(no != NULL){
        cont++;
        no = no->prox;
    }
    return cont;
}

int Pilha_cheia(Pilha* pi){
    return 0;
}

int Pilha_vazia(Pilha* pi){
    if(pi == NULL)
        return 1;
    if(*pi == NULL)
        return 1;
    return 0;
}

void imprime_Pilha(Pilha* pi){
    if(pi == NULL)
        return;
    Elem* no = *pi;
    while(no != NULL){
        printf("%c\n",no->valor);
        printf("---------\n");
        no = no->prox;
    }
}

void infixa_para_prefixa(char infixa[30], char prefixa[30]) {
    Pilha * pi = cria_Pilha();
    insere_Pilha(pi, '#');
    char temp[30];

    int i,j=0;
	char operando, ref;
	inverte_str(infixa);

	for (i=0; i<strlen(infixa); i++) {
		operando=infixa[i];
		if (tem_operando(operando)==0) {
			temp[j]=operando;
			j++;
		} 
        else {
            if (operando == ')') {
                insere_Pilha(pi, operando);
            }
            else if(operando == '(') {
                while (topo_pilha(pi)!=')') {
                    consulta_topo_Pilha(pi, &ref);
                    temp[j]=ref;
                    remove_Pilha(pi);
                    j++;
                }
                consulta_topo_Pilha(pi, &ref);
                temp[j]=ref;
                remove_Pilha(pi);
            } 
            else {
                if (prioridade_op(topo_pilha(pi)) <= prioridade_op(operando)) {
                    insere_Pilha(pi, operando);
                } else {
                    while(prioridade_op(topo_pilha(pi)) >= prioridade_op(operando)) {
                        consulta_topo_Pilha(pi, &ref);
                        temp[j]=ref;
                        remove_Pilha(pi);
                        j++;
                    }
                    insere_Pilha(pi, operando);
                }
            }
		}
	}

	while (topo_pilha(pi)!='#') {
        consulta_topo_Pilha(pi, &ref);
        temp[j]=ref;
        remove_Pilha(pi);
		++j;
	}

	temp[j]='\0';
    inverte_str(temp);
    strcpy(prefixa, temp);
}

void infixa_para_posfixa(char infixa[30], char posfixa[30]) {
    Pilha * pi = cria_Pilha();
    insere_Pilha(pi, '#');
    char temp[30];

    int i=0,j=0;
    char operando, ref;

    for (i=0; i<strlen(infixa); i++) {
        operando=infixa[i];
        if(tem_operando(operando)==0)  {
            temp[j]=operando;
            j++;
        } 
        else {
            if (operando == '(') {
                insere_Pilha(pi, operando);
            }
            else if(operando == ')') {
                while(topo_pilha(pi) != '(') {
                    consulta_topo_Pilha(pi, &ref);
                    temp[j]=ref;
                    remove_Pilha(pi);
                    j++;
                }
                remove_Pilha(pi);
            } 
            else {
                while(prioridade_posfx(topo_pilha(pi)) >= prioridade_posfx(operando)) {
                    consulta_topo_Pilha(pi, &ref);
                    temp[j]=ref;
                    remove_Pilha(pi);
                    j++;
                }
                insere_Pilha(pi, operando);
            }
        }
    }
    while(topo_pilha(pi) != '#') {
        consulta_topo_Pilha(pi, &ref);
        temp[j]=ref;
        remove_Pilha(pi);
        j++;
    }
    
    temp[j]='\0';
    strcpy(posfixa, temp);
}

void inverte_str(char str[30]) {
	char temp[30];
    int i, j;
	for (i=(strlen(str)-1), j=0; i+1!=0; --i, ++j) {
		temp[j]=str[i];
	}
	temp[j]='\0';
	strcpy(str,temp);
}

int prioridade_op(char carac) {
	switch(carac) {
		case '+':
		        case '-':
		        return 2;
		break;
		case '*':
		        case '/':
		        return 4;
		break;
		case '$':
		        case '^':
		        return 6;
		break;
		case '#':
		        case '(':
		        case ')':
		        return 1;
		break;
	}
}

int tem_operando(char carac) {
	switch(carac) {
		case '+':
		        case '-':
		        case '*':
		        case '/':
		        case '^':
		        case '$':
		        case '&':
		        case '(':
		        case ')':
		        return 1;
		break;
		default:
            return 0;
	}
}

int prioridade_posfx(char carac) {
    switch(carac) {
        case '#': 
            return 0;
        case '(': 
            return 1;
        case '+':
            case '-': 
            return 2;
        case '*':
            case '/': 
            return 3;
    }
}
