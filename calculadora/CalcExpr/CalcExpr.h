struct elemento{
    char valor;
    struct elemento *prox;
};
typedef struct elemento Elem;
typedef struct elemento* Pilha;


struct elementoint{
    int valor;
    struct elementoint *prox;
};
typedef struct elementoint ElemInt;

typedef struct elementoint* PilhaInt;

Pilha* cria_Pilha();
void libera_Pilha(Pilha* pi);
int consulta_topo_Pilha(Pilha* pi, char *carac);
char topo_pilha(Pilha* pi);
int insere_Pilha(Pilha* pi, char carac);


PilhaInt* cria_PilhaInt();
int controle_calculadora(char *str, PilhaInt* pilha_operacao, PilhaInt* pilha_numeros);
void inserePilha_calculadora(PilhaInt* pi,  int item);
int logica_calculadora(PilhaInt* pilha_num, PilhaInt* pilha_op, int count_op, int isexpo);
int retornaNumeroPilha_calculadora(PilhaInt* pilha_num);
char retornaCharPilha_calculadora(PilhaInt* pilha_op);
void validacao_numerica(char *str );

int remove_Pilha(Pilha* pi);
int tamanho_Pilha(Pilha* pi);
int Pilha_vazia(Pilha* pi);
int Pilha_cheia(Pilha* pi);
void imprime_Pilha(Pilha* pi);
void infixa_para_prefixa(char infixa[30], char prefixa[30]);
void infixa_para_posfixa(char infixa[30], char posfixa[30]);
void inverte_str(char str[30]);
int prioridade_op(char carac);
int prioridade_posfx(char carac);
int tem_operando(char carac);
