#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include "CalcExpr/CalcExpr.h"

int main() {
  setlocale(LC_ALL, "");
  int i, result, opt1 = -1;
  char entrada[100], prefixa[30], posfixa[30];

  while (opt1 != 0) {
    printf("[1] Transforma expressao Infixa em Posfixa\n");
    printf("[2] Transforma expressao Infixa em Prefixa\n");
    printf("[3] Le uma expressao e informa se a expressao e valida\n");
    printf("[4] Le uma expressao e informa o resultado da expressao\n");
    printf("[0] Sair\n");
    scanf("%d", & opt1);
    getchar();

    switch (opt1) {
    case 0:
      return 0;

    case 1:
      system("cls");
      printf("---- EXPRESSAO INFIXA EM POSFIXA ----\n\n");
      printf("Digite a expressao infixa para transformacao:\n");
      fgets(entrada, 30, stdin);
      strtok(entrada, "\n");
      infixa_para_posfixa(entrada, posfixa);
      printf("Resultado Posfixado: \n");
      printf("%s\n", posfixa);
      break;

    case 2:
      system("cls");
      printf("---- EXPRESSAO INFIXA EM PREFIXA ----\n\n");
      printf("Digite a expressao infixa para transformacao:\n");
      fgets(entrada, 100, stdin);
      strtok(entrada, "\n");
      infixa_para_prefixa(entrada, prefixa);
      printf("Resultado Prefixado: \n");
      printf("%s\n",prefixa);
      break;

    case 3:
      system("cls");
      printf("---- VERIFICACAO DE VALIDADE DE EXPRESSAO ----\n\n");
      printf("Digite a expressao infixa para verificar validade:\n");
      fgets(entrada, 100, stdin);

      validacao_numerica(entrada);

      break;

    case 4:
      system("cls");
      PilhaInt * pilha_num = cria_PilhaInt();
      PilhaInt * pilha_op = cria_PilhaInt();

      printf("---- CALCULADORA DE EXPRESSAO ----\n\n");
      printf("Digite a expressao infixa para verificar seu resultado:\n");
      fgets(entrada, 100, stdin);
      result = controle_calculadora(entrada, pilha_op, pilha_num);
      printf("O resultado e: %d \n\n\n", result);
      break;

    }

  }

  system("pause");
  return 0;
}
