#include <stdio.h>
#include <stdlib.h>
#include "FilaImpressao/FilaImpressao.h"

int main() {
  printf("EDI \nFILA DE IMPRESSAO \nMARIO NETO E ROGERIO BAYER\n\n");
  FilaImpr * fila_entrada = cria_FilaImpr();
  FilaImpr * fila_0 = cria_FilaImpr(); //PRIORIDADE MAXIMA
  FilaImpr * fila_1 = cria_FilaImpr(); //PRIORIDADE MEDIA
  FilaImpr * fila_2 = cria_FilaImpr(); //PRIORIDADE BAIXA
  printf("Filas Iniciadas  \nzn");

  leituraArquivo_FilaImpr(fila_entrada);
  int i;

  printf("Fila Principal - ARQUIVO  \n");
  printf("Prioridade:\tIP:\n");
  imprime_FilaImpr(fila_entrada);

  controlePrioridades_FilaImpr(fila_entrada, fila_0, fila_1, fila_2);

  system("pause");
  libera_FilaImpr(fila_entrada);
  libera_FilaImpr(fila_0);
  libera_FilaImpr(fila_1);
  libera_FilaImpr(fila_2);
  printf("Filas Liberadas\n\n");

  return 0;
}
