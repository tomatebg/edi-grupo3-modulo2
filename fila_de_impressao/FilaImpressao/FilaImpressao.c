#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "FilaImpressao.h"

FilaImpr * cria_FilaImpr() {
  FilaImpr * fi = (FilaImpr * ) malloc(sizeof(FilaImpr));
  if (fi != NULL) {
    fi -> final = NULL;
    fi -> inicio = NULL;
    fi -> qtd = 0;
  }
  return fi;
}

void libera_FilaImpr(FilaImpr * fi) {
  if (fi != NULL) {
    Elem * no;
    while (fi -> inicio != NULL) {
      no = fi -> inicio;
      fi -> inicio = fi -> inicio -> prox;
      free(no);
    }
    free(fi);
  }
}

int leituraArquivo_FilaImpr(FilaImpr * fp) {
  FILE * arq;

  if ((arq = fopen("fila.txt", "r")) == NULL) {
    printf("Erro ao abrir o arquivo.\n");
    return 0;
  }

  struct impressao leitura;
  char c[5];
  printf("Lendo Arquivo...\n");

  for (int i = 0; !feof(arq); i++) {
    fscanf(arq, "%[^,], %[^;]; ", & c, leitura.ip);
    leitura.prio = atoi(c);
    insere_FilaImpr(fp, leitura);
  }
  return 1;
}

int consulta_FilaImpr(FilaImpr * fi, struct impressao * im) {
  struct impressao result;
  if (fi == NULL)
    return 0;
  if (fi -> inicio == NULL) //fila vazia
    return 0;
  * im = fi -> inicio -> documento;
  return 1;
}

int insere_FilaImpr(FilaImpr * fi, struct impressao im) {
  if (fi == NULL)
    return 0;
  Elem * no = (Elem * ) malloc(sizeof(Elem));
  if (no == NULL)
    return 0;
  no -> documento = im;
  no -> prox = NULL;
  if (fi -> final == NULL) //fila vazia
    fi -> inicio = no;
  else
    fi -> final -> prox = no;
  fi -> final = no;
  fi -> qtd++;
  return 1;
}

int remove_FilaImpr(FilaImpr * fi) {
  if (fi == NULL)
    return 0;
  if (fi -> inicio == NULL)
    return 0;
  Elem * no = fi -> inicio;
  fi -> inicio = fi -> inicio -> prox;
  if (fi -> inicio == NULL)
    fi -> final = NULL;
  free(no);
  fi -> qtd--;
  return 1;
}

int tamanho_FilaImpr(FilaImpr * fi) {
  if (fi == NULL)
    return 0;
  return fi -> qtd;
}

int FilaImpr_vazia(FilaImpr * fi) {
  if (fi == NULL)
    return 1;
  if (fi -> inicio == NULL)
    return 1;
  return 0;
}

int FilaImpr_cheia(FilaImpr * fi) {
  return 0;
}

void imprimeListas_FilaImpr(FilaImpr * fp, FilaImpr * f0, FilaImpr * f1, FilaImpr * f2) {

  printf("Fila Principal  \n");
  printf("Prioridade:\tIP:\n");

  imprime_FilaImpr(fp);

  printf("Fila de Prioridade Maxima \n");
  printf("Prioridade:\tIP:\n");
  imprime_FilaImpr(f0);

  printf("Fila de Prioridade Normal \n");
  printf("Prioridade:\tIP:\n");
  imprime_FilaImpr(f1);

  printf("Fila de Prioridade Baixa \n");
  printf("Prioridade:\tIP:\n");
  imprime_FilaImpr(f2);

}

int imprime_FilaImpr(FilaImpr * fi) {

  int i = 0;
  if (fi == NULL)
    return i;
  Elem * no = fi -> inicio;
  while (no != NULL) {
    printf("    %d\t\t", no -> documento.prio);
    printf("%s\n", no -> documento.ip);
    i++;
    no = no -> prox;
  }
  printf("--------%d documentos----------\n\n\n", i);

  return i;
}

void delay(float segundos) {
  int milli_seconds = 1000 * segundos;
  clock_t start_time = clock();
  while (clock() < start_time + milli_seconds);
}

void controlePrioridades_FilaImpr(FilaImpr * fp, FilaImpr * f0, FilaImpr * f1, FilaImpr * f2) {
  struct impressao consulta;
  int count = 1;

  while (FilaImpr_vazia(fp) == 0) {
    printf("------------------ Ciclo %d --------------\n", count);

    for (int i = 0; i < 5; i++) {
      if (FilaImpr_vazia(fp) == 1) break;
      consulta_FilaImpr(fp, & consulta);

      switch (consulta.prio) {
      case 1:
        insere_FilaImpr(f0, consulta);
        remove_FilaImpr(fp);
        break;

      case 2:
        insere_FilaImpr(f1, consulta);
        remove_FilaImpr(fp);
        break;

      case 3:
        insere_FilaImpr(f2, consulta);
        remove_FilaImpr(fp);
        break;
      }
    }

    imprimeListas_FilaImpr(fp, f0, f1, f2);
    count++;
    delay(0.5);
  }

};
