
struct impressao {
  char ip[30];
  int prio;
};

//FilaImpr
struct elemento {
  struct impressao documento;
  struct elemento * prox;
};
typedef struct elemento Elem;

//n� descritor
struct fila {
  struct elemento * inicio;
  struct elemento * final;
  int qtd;
};

typedef struct fila FilaImpr;

FilaImpr * cria_FilaImpr();
void libera_FilaImpr(FilaImpr * fi);
int consulta_FilaImpr(FilaImpr * fi, struct impressao * im);
int leituraArquivo_FilaImpr(FilaImpr * fp);
int insere_FilaImpr(FilaImpr * fi, struct impressao im);
int remove_FilaImpr(FilaImpr * fi);
int tamanho_FilaImpr(FilaImpr * fi);
int FilaImpr_vazia(FilaImpr * fi);
int FilaImpr_cheia(FilaImpr * fi);
int imprime_FilaImpr(FilaImpr * fi);
void imprimeListas_FilaImpr(FilaImpr * fp, FilaImpr * f0, FilaImpr * f1, FilaImpr * f2);
void controlePrioridades_FilaImpr(FilaImpr * fp, FilaImpr * f0, FilaImpr * f1, FilaImpr * f2);
void delay(float segundos);
